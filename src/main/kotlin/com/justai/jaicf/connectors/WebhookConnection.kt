package com.justai.jaicf.connectors

import com.justai.jaicf.channel.http.httpBotRouting
import com.justai.jaicf.channel.jaicp.JaicpWebhookConnector
import com.justai.jaicf.channel.jaicp.channels.ChatApiChannel
import com.justai.jaicf.channel.jaicp.channels.ChatWidgetChannel
import com.justai.jaicf.channel.jaicp.channels.TelephonyChannel
import com.justai.jaicf.channel.yandexalice.AliceChannel

import com.justai.jaicf.accessToken
import com.justai.jaicf.templateBot
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

fun main() {
    embeddedServer(Netty, System.getenv("PORT")?.toInt() ?: 8080) {
        routing {
            httpBotRouting(
                "/" to JaicpWebhookConnector(
                    templateBot,
                    accessToken,
                    channels = listOf(ChatApiChannel, ChatWidgetChannel, TelephonyChannel)
                )
            )
        }
    }.start(wait = true)
}