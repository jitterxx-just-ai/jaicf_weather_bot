package com.justai.jaicf.scenario

import com.justai.jaicf.channel.jaicp.channels.TelephonyEvents
import com.justai.jaicf.channel.jaicp.dto.telephony
import com.justai.jaicf.helpers.logging.WithLogger

import com.justai.jaicf.activator.caila.caila
import com.justai.jaicf.channel.googleactions.actions
import com.justai.jaicf.context.BotContext
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.City
import com.justai.jaicf.Datetime
import com.justai.jaicf.channel.jaicp.reactions.telephony
import com.justai.jaicf.channel.telegram.telegram
import com.justai.jaicf.channel.yandexalice.alice
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.get
import io.ktor.util.KtorExperimentalAPI
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.content

@KtorExperimentalAPI
object MainScenario : Scenario(), WithLogger {

    class Weather(context: BotContext) {
        var lat: Double? by context.temp
        var lon: Double? by context.temp
        var res: JsonObject? by context.temp
        var temperature: String? by context.temp
        var weather: String? by context.temp
        var name: String? by context.temp
        var feelsLike: String? by context.temp
        var timestamp: Long? by context.temp
    }

    private val json = Json(JsonConfiguration.Stable.copy(strictMode = false, encodeDefaults = false))
    @KtorExperimentalAPI
    val httpClient = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    init {
        state("/ringing") {
            globalActivators {
                event(TelephonyEvents.ringing)
            }
            action {
                request.telephony?.let {
                    logger.debug("Incoming call from ${it.caller}")
                }
            }
        }

        state("main") {
            activators {
                regex("start")
                regex("привет")
            }

            action {
                reactions.say("Привет!")
                reactions.telephony?.say("Это телефонный канал.")
                reactions.alice?.say("Это приватный навык.")
                reactions.telegram?.say("Я погодный бот, рассказываю о погоде в разных городах мира.")
                reactions.alice?.say("Я погодный бот. Погоду когда и в каком городе ты хочешь узнать?")
            }
        }

        state("Help") {
            activators {
                intent("Help")
            }

            action {
                reactions.say("Я погодный бот, рассказываю о погоде в разных городах мира.")
                reactions.say("Назови город и на какой день нужен прогноз ))")
            }
        }

        state("Stop") {
            activators {
                intent("Stop")
            }

            action {
                reactions.sayRandom("Ок", "Хорошо")
                reactions.alice?.endSession()
                reactions.telephony?.hangup()
            }
        }

        state("GetWeather") {
            activators {
                intent("GetWeather")
            }
            action {
                val weather = Weather(context)
                activator.caila?.run {
                    val cityValue = json.parse(City.serializer(), slots["cityname"] ?: error("Got a Null"))
                    weather.timestamp = if (slots["forDay"] != null)  json.parse(Datetime.serializer(), slots["день"] ?: error ("Got a Null")).timestamp/1000 + 36000 else null
                    weather.lat = cityValue.lat
                    weather.lon = cityValue.lon
                    weather.name = cityValue.name
                    weather.res = runBlocking {
                        httpClient.get<JsonObject>("http://api.openweathermap.org/data/2.5/onecall?APPID=2c0cd08ff3c8bba3da779830aabb1823&part=current,daily&units=metric&lat=${weather.lat}&lon=${weather.lon}&lang=ru")
                    }
                    if (weather.timestamp == null) {
                        weather.temperature = weather.res!!["current"]?.jsonObject?.get("temp")?.content
                        weather.feelsLike = weather.res!!["current"]?.jsonObject?.get("feels_like")?.content
                        weather.weather = weather.res!!["current"]?.jsonObject?.get("weather")?.jsonArray?.get(0)?.jsonObject?.get("description")?.content
                        reactions.say("В городе ${weather.name} сейчас температура ${weather.temperature} градусов, ощущается как ${weather.feelsLike}, ${weather.weather}.")
                    } else {
                        val dailyJson = weather.res!!["daily"]?.jsonArray
                        if (dailyJson != null) {
                            for (i in dailyJson) {
                                val timestamp = i.jsonObject["dt"]?.content?.toLong()
                                if (timestamp == weather.timestamp) {
                                    weather.temperature = i.jsonObject["temp"]?.jsonObject?.get("day")?.content
                                    weather.feelsLike = i.jsonObject["feels_like"]?.jsonObject?.get("day")?.content
                                    weather.weather = i.jsonObject["weather"]?.jsonArray?.get(0)?.jsonObject?.get("description")?.content
                                    break
                                }
                            }
                            if (weather.temperature != null){
                                reactions.say("В городе ${weather.name} на этот день будет температура ${weather.temperature} градусов, ощущается как ${weather.feelsLike}, ${weather.weather}.")
                            } else {
                                reactions.say("Кажется, на этот день прогноза нет. Попробуй спросить че полегче, например погоду на сегодня или на пару дней вперед")
                            }
                        } else {
                            reactions.say("Кажется, на этот день прогноза нет. Попробуй спросить че полегче, например погоду на сегодня или на пару дней вперед")
                        }
                    }
                }
            }
        }

        state("/speechNotRecognized") {
            globalActivators {
                event(TelephonyEvents.speechNotRecognized)
            }
            action {
                reactions.sayRandom(
                    "Извините, вас не слышно. Повторите, пожалуйста...",
                    "Повторите, пожалуйста, плохо слышно.",
                    "Повторите, пожалуйста..."
                )
            }
        }

        state("/catchHangup") {
            globalActivators {
                event(TelephonyEvents.hangup)
            }
            action {
                request.telephony?.let {
                    logger.info("Conversation ended with caller: ${it.caller}")
                }
            }
        }

        state("fallback", noContext = true) {
            activators {
                catchAll()
            }

            action {
                reactions.say("Назови город, и я дам прогноз на сегодня.")
                reactions.go("/GetWeather")

            }
        }
    }
}