package com.justai.jaicf

import com.justai.jaicf.BotEngine
import com.justai.jaicf.activator.caila.CailaIntentActivator
import com.justai.jaicf.activator.caila.CailaNLUSettings
import com.justai.jaicf.activator.catchall.CatchAllActivator
import com.justai.jaicf.channel.googleactions.dialogflow.ActionsDialogflowActivator
import com.justai.jaicf.context.manager.InMemoryBotContextManager
import com.justai.jaicf.context.manager.mongo.MongoBotContextManager
import com.justai.jaicf.scenario.MainScenario
import com.justai.jaicf.scenario.TelephonyBotScenario
import com.mongodb.MongoClient
import com.mongodb.MongoClientURI
import com.justai.jaicf.activator.caila.caila
import com.justai.jaicf.activator.caila.dto.CailaIntent
import com.justai.jaicf.activator.regex.RegexActivator
import com.justai.jaicf.activator.event.BaseEventActivator


private val contextManager = System.getenv("MONGODB_URI")?.let { url ->
    val uri = MongoClientURI(url)
    val client = MongoClient(uri)
    MongoBotContextManager(client.getDatabase(uri.database!!).getCollection("contexts"))

} ?: InMemoryBotContextManager

val CailaActivator = CailaIntentActivator.Factory(cailaNLUSettings)

val templateBot = BotEngine(
    model = TelephonyBotScenario.model,
    contextManager = contextManager,
    activators = arrayOf(
        CailaActivator,
        RegexActivator,
        CatchAllActivator,
        BaseEventActivator
    )
)
