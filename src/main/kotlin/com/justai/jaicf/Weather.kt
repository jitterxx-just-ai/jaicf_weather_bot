package com.justai.jaicf

import kotlinx.serialization.Serializable

@Serializable
data class Weather (
    val temp : String
)