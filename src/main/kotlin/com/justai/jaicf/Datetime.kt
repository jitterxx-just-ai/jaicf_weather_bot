package com.justai.jaicf

import kotlinx.serialization.Serializable

@Serializable
data class Datetime(
    val timestamp : Long
)